// {{CopyrightNotice}}
package com.iptor.training.application;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;


/**
 * The <code>CustomerApplication</code> class  
 */
@ApplicationPath("api")
public class TrainingApplication extends ResourceConfig{

	/**
	 * Initializes a newly created <code>CustomerApplication</code>
	 *
	 */
	public TrainingApplication(){
		//this.packages("com.iptor.training.resources","org.codehaus.jackson.jaxrs");
		this.packages("com.iptor.training.resources");
	}

}
