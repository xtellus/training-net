/**
 * 
 */
package com.iptor.training.resources;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.iptor.training.model.Person;


/**
 *
 *
 */
@Path("training")
public class TrainingResource {

	@GET
	@Path("test/{name}")
	public Response test(@PathParam("name") String name){
		String s = "Hello "+name+"!";
		return Response.ok().entity(s).build();
	}
	
	
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON + ";charset=utf-8" })
	@Path("test2/{name}/{surName}")
	public Response test2(@PathParam("name") String name,@PathParam("surName") String surName){
		Person p = new Person(name,surName);
		return Response.ok().entity(p).build();
	}
	
	/*
	@GET
	@Path("price/{cuno}/{item}")
	public Response getPrice(@PathParam("cuno") String cuno,@PathParam("item") String item){
		GetPrice gp = new GetPrice();
		String price = gp.getPrice(cuno, item);
		return Response.ok().entity(price).build();
	}
	
	@GET
	@Path("search/{arg}")
	public Response search(@PathParam("arg") String arg){
		try {
			SearchDocuments sd = new SearchDocuments();
			String s =sd.search(arg);
			if(s == null)
				return Response.serverError().build();
			
			return Response.ok().entity(s).build();			
		} catch (Exception e) {
			return Response.serverError().entity(e.getMessage()).build();
			
		}

	}
	*/
}
